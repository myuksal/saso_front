import { Injectable } from '@angular/core';
import { AJAXService } from '@sago/Services/ajax.service'

@Injectable({
  providedIn: 'root'
})
export class SliderService {

  constructor(
    private ajax_Services:AJAXService
  ) { 
    
  }


  ajax_Get_Home():Promise<any> {
    return new Promise( (resolve, reject) => {

      this.ajax_Services.F_AJAX('home').then(home_response => {
        resolve(home_response);        
      }, reject);
    

    });
  }


}
