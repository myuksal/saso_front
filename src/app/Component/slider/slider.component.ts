import { Component, ViewChild, HostListener, Inject } from '@angular/core';

import { SliderService } from './slider.service'
import { AJAXService } from '@sago/Services/ajax.service'
import { DragScrollComponent } from 'ngx-drag-scroll/lib';

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.scss']
})
export class SliderComponent {

  @ViewChild('image_scroll', {read: DragScrollComponent}) drag_scroll: DragScrollComponent;

  slider_Images:any[];

  constructor(
    private Slider_Services:SliderService,
    private ajax_Services:AJAXService,
  ){

    this.ajax_Services.F_AJAX('home/').then(home_response => {
      this.slider_Images = home_response; 
    });

  }


  moveLeft() {
    this.drag_scroll.moveLeft();
  }
  moveRight() {
    this.drag_scroll.moveRight();
  }
  test() {
    alert("sdfsadf");
  }
  
}
