import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'photos-grid',
  templateUrl: './photos.component.html',
  styleUrls: ['./photos.component.scss'],
  inputs: ['items']
})
export class PhotosComponent implements OnInit {

  constructor(public dialog: MatDialog) { }

  ngOnInit() {
  }


  openMore(item){
    const dialogRef = this.dialog.open(showMore_Popup, {
      width: '99%',
      height:'2000px',
      data: item
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }
  openNewTab(item){
    window.open("/photo/one/"+item.index, "_blank");
  }
}

@Component({
  selector: 'showMore_Popup',
  styleUrls: ['showMore_Popup.scss'],
  templateUrl: 'showMore_Popup.html',
})
export class showMore_Popup {

  constructor(
    public dialogRef: MatDialogRef<showMore_Popup>,
    @Inject(MAT_DIALOG_DATA) public data: More_Data) {}

    popup_close(): void {
      this.dialogRef.close();
    }

}


interface More_Data{
  "index": number,
  "upload_time": string,
  "user": {
      "nickname": string,
      "image_thumbnail": string,
  },
  "iso": number,
  "shutter_speed": string,
  "aperture": string,
  "content": string,
  "photo_thumbnail": string,
  "photo_show": string,
}