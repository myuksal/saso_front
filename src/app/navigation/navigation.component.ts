import { Component, OnInit } from '@angular/core';
import { MatIconRegistry } from "@angular/material/icon";
import { DomSanitizer } from "@angular/platform-browser";

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component_new.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {

  constructor(
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer
  ) { 
    this.matIconRegistry.addSvgIcon(
      `aecom`,
      this.domSanitizer.bypassSecurityTrustResourceUrl("https://discordapp.com/assets/e4923594e694a21542a489471ecffa50.svg")
    );
    this.matIconRegistry.addSvgIcon(
      `aecom_small`,
      this.domSanitizer.bypassSecurityTrustResourceUrl("https://s3.ap-northeast-2.amazonaws.com/myuksal-saso/svg/Discord-Logo-Color.svg")
    );
   }

  ngOnInit() {
  }

}
