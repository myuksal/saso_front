import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {HomeComponent} from './home/home.component';
import {MembersComponent} from './members/members.component';
import {AboutComponent} from './about/about.component';
import {PhotoAllComponent} from './photos/all/all.component';
import {PhotoOwnComponent} from './photos/onepicture/onepicture.component';
import {FileUploaderComponent} from './Component/file-uploader/file-uploader.component';

const routes: Routes = [
  {path: 'home',        component: HomeComponent},
  {path: 'members',     component: MembersComponent},
  {path: 'about',       component: AboutComponent},

  {path: 'photo/:order_type',       component: PhotoAllComponent},
  {path: 'photo/user/:user',       component: PhotoAllComponent},
  {path: 'photo/one/:id',       component: PhotoOwnComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
