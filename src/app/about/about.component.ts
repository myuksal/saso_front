import { Component, OnInit } from '@angular/core';
import { AJAXService } from '@sago/Services/ajax.service'

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {
  
  abouts: any[];

  constructor(
    private ajax_Services:AJAXService,
  ) { }
    
    
  ngOnInit() {
    this.ajax_Services.F_AJAX('about').then(about_response => {
      this.abouts = about_response;
    });
  }

}
