import { Component, OnInit } from '@angular/core';
import { AJAXService } from '@sago/Services/ajax.service'

@Component({
  selector: 'app-members',
  templateUrl: './members.component_old.html',
  styleUrls: ['./members.component.scss']
})
export class MembersComponent implements OnInit {
  users: users_interface[];
  constructor(
    private ajax_Services:AJAXService,
  ) { 
    this.ajax_Services.F_AJAX('users/').then(members_response => {
      this.users = members_response; 
    });
  }

  

  ngOnInit() {
  }

}


interface users_interface{
  "nickname": string,
  "image_thumbnail": string,
  "me": string,
  "facebook": string,
  "instargram": string,
  "youtube": string
}