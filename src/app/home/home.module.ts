import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SliderComponent } from '../Component/slider/slider.component'
import { DragScrollModule } from 'ngx-drag-scroll';
import { 
  MatToolbarModule,
  MatIconModule,
  MatButtonModule,
  MatCardModule
 } from '@angular/material';

import {HttpClient, HttpClientModule} from '@angular/common/http';



@NgModule({
  declarations: [
    SliderComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    MatCardModule,
  ],
  bootstrap: [
    SliderComponent
  ]
})
export class HomeModule { }
 