import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { 
  MatToolbarModule,
  MatIconModule,
  MatButtonModule,
  MatCardModule,
  MatFormFieldModule,
  MatOptionModule,
  MatSelectModule,
  MatDialogModule,
  MatBottomSheetModule,
  MatSnackBarModule,
  MatProgressBarModule,
  MatProgressSpinnerModule
 } from '@angular/material';
import { NgxMasonryModule } from 'ngx-masonry';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavigationComponent } from './navigation/navigation.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SliderComponent } from './Component/slider/slider.component';1

import { DragScrollModule } from 'ngx-drag-scroll';

import { HttpClient, HttpClientModule } from '@angular/common/http';
import { FileUploaderComponent } from './Component/file-uploader/file-uploader.component'
import { PhotosComponent, showMore_Popup } from './Component/photos/photos.component';

import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {FileSelectDirective} from "ng2-file-upload";
import { HomeComponent } from './home/home.component';
import { MembersComponent } from './members/members.component';
import { AboutComponent } from './about/about.component';
import { PhotoAllComponent } from './photos/all/all.component';
import { PhotoOwnComponent, ShareSheet, CopyDone } from './photos/onepicture/onepicture.component';


@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    HomeComponent,
    SliderComponent,
    FileUploaderComponent,
    FileSelectDirective,
    MembersComponent,
    AboutComponent,
    PhotoAllComponent,
    PhotosComponent,
    showMore_Popup,
    PhotoOwnComponent,
    ShareSheet,
    CopyDone
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    DragScrollModule,



    //Material Module
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    MatCardModule,
    MatFormFieldModule,
    MatOptionModule,
    MatSelectModule,
    MatDialogModule,
    MatBottomSheetModule,
    MatSnackBarModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,

    //NgxMasonryModule
    NgxMasonryModule,
    
    //InfiniteScrollModule
    InfiniteScrollModule,


    BrowserAnimationsModule,

    //Form Module
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [
    showMore_Popup,
    ShareSheet,
    CopyDone
  ]
})
export class AppModule { }
