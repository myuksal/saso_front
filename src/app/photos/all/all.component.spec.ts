import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PhotoAllComponent } from './all.component';

describe('AllComponent', () => {
  let component: PhotoAllComponent;
  let fixture: ComponentFixture<PhotoAllComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PhotoAllComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhotoAllComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
