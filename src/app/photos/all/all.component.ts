import { Component, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AJAXService } from '@sago/Services/ajax.service'

@Component({
  selector: 'app-photo',
  templateUrl: './all.component.html',
  styleUrls: ['./all.component.scss'],
})
export class PhotoAllComponent {

  xurl: string;

  loading = false;

  photos: photosInterface[];
  next_page : string;
  now = 0;

  throttle = 50;
  scrollDistance = 2;
  direction = '';

  constructor(
    route : ActivatedRoute,
    private ajax_Services:AJAXService,
  ) { 

    

    this.xurl = route.snapshot.params['order_type'];
    if(route.snapshot.params['user']){
      var nickname = route.snapshot.params['user'];
      this.xurl = "userpost/?nickname="+nickname;
    }
    
  }

 

  ngOnInit() {
    this.ajax_Services.F_AJAX(this.xurl).then(first_photo_response => {
      this.photos = first_photo_response.results;
      this.next_page = first_photo_response.next;
    });
  }


  ScrollDown(ev){
    if(this.next_page){
      this.ajax_Services.F_AJAX_original(this.next_page).then(more_photo_response => {
        this.sleep(90).then(r =>{
          this.photos=this.photos.concat(more_photo_response.results);
        });
        this.next_page = more_photo_response.next

      });
    }
    if(this.next_page){
      this.loading = false;
    }
  }

  sleep(milliseconds):Promise<any> {

    return new Promise((resolve, reject) => {
      var start = new Date().getTime();
      for (var i = 0; i < 1e7; i++) {
        if ((new Date().getTime() - start) > milliseconds){
          break;
        }
      }
      resolve('done');
    });
  }
}


interface user {
  nickname:string;
  image_thumbnail:string;
}

interface photosInterface {
  index:number;
  upload_time:string;
  iso:number;
  shutter_speed:string;
  aperture:string;
  content:string;
  photo_thumbnail:string;
  photo_show:string;
  user:user;
}