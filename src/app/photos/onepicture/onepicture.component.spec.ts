import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PhotoOwnComponent } from './onepicture.component';

describe('OnepictureComponent', () => {
  let component: PhotoOwnComponent;
  let fixture: ComponentFixture<PhotoOwnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PhotoOwnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhotoOwnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
