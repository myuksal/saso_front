import { Component, OnInit, Inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AJAXService } from '@sago/Services/ajax.service'

import {MatBottomSheet, MatBottomSheetRef, MAT_BOTTOM_SHEET_DATA, MatSnackBar} from '@angular/material';
import { MatIconRegistry } from "@angular/material/icon";
import { DomSanitizer } from "@angular/platform-browser";
import { getUrlScheme } from '@angular/compiler';

@Component({
  selector: 'app-onepicture',
  templateUrl: './onepicture.component.html',
  styleUrls: ['./onepicture.component.scss']
})
export class PhotoOwnComponent implements OnInit {

  photo: photosInterface[];

  constructor(
    route : ActivatedRoute,
    private ajax_Services:AJAXService,
    private bottomSheet: MatBottomSheet,
  ) { 

    this.ajax_Services.F_AJAX('own/?id='+route.snapshot.params['id']).then(photo_response => {
      this.photo = photo_response[0];
    });

  }

  ngOnInit() {
  }


  openShareSheet(id){
    this.bottomSheet.open(ShareSheet, {
      data: this.photo
    });
  }
  closeWin() {
    window.close();   // Closes the new window
  }

}




@Component({
  selector: 'share-sheet',
  templateUrl: 'share-sheet.html',
})
export class ShareSheet {
  constructor(
    private bottomSheetRef: MatBottomSheetRef<ShareSheet>,
    private snackBar: MatSnackBar,
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer,
    @Inject(MAT_BOTTOM_SHEET_DATA) public item
    ) {
      this.addIcon('google_plus','https://s3.ap-northeast-2.amazonaws.com/myuksal-saso/svg/iconfinder_google_plus_986946.svg');
      this.addIcon('facebook','https://s3.ap-northeast-2.amazonaws.com/myuksal-saso/svg/iconfinder_square-facebook_317727.svg');
      this.addIcon('twitter','https://s3.ap-northeast-2.amazonaws.com/myuksal-saso/svg/iconfinder_twitter_square_107066.svg');
      this.addIcon('naver','https://s3.ap-northeast-2.amazonaws.com/myuksal-saso/svg/%EB%84%A4%EC%9D%B4%EB%B2%84+%EC%95%84%EC%9D%B4%EB%94%94%EB%A1%9C+%EB%A1%9C%EA%B7%B8%EC%9D%B8_%EC%95%84%EC%9D%B4%EC%BD%98%ED%98%95_Green.PNG');
    }

  openLink(event: MouseEvent): void {
    this.bottomSheetRef.dismiss();
    event.preventDefault();
  }


  addIcon(title, xUrl){
    this.matIconRegistry.addSvgIcon(
      title,
      this.domSanitizer.bypassSecurityTrustResourceUrl(xUrl)
    );
  }


  getUrl(id){
    return "https://projectsago.com/photo/one/"+id;
  }

  Share_Naver(){
    window.open("http://share.naver.com/web/shareView.nhn?url="+this.getUrl(this.item.index)+"&title="+this.item.content, "_blank", "width=400,height=500,left=300, location=no");
  }
  Share_Google(){
    window.open("https://plus.google.com/share?url="+this.getUrl(this.item.index), "_blank", "width=400,height=500,left=300, location=no");
  }
  Share_Facebook(){
    window.open("https://www.facebook.com/sharer/sharer.php?u="+this.getUrl(this.item.index)+"&t="+this.item.content, "_blank", "width=400,height=500,left=300, location=no");
  }
  Share_Twitter(){
    window.open("https://twitter.com/intent/tweet?text=[%EA%B3%B5%EC%9C%A0]%20"+this.getUrl(this.item.index)+"%20-%20"+this.item.content, "_blank", "width=400,height=500,left=300, location=no");
  }
  Share_URL(){
    this.copyToClipboard(this.getUrl(this.item.index));
    this.snackBar.openFromComponent(CopyDone, {
      duration: 500,
    });
  }

  copyToClipboard(item) {
    document.addEventListener('copy', (e: ClipboardEvent) => {
      e.clipboardData.setData('text/plain', (item));
      e.preventDefault();
      document.removeEventListener('copy', null);
    });
    document.execCommand('copy');
  }

}



@Component({
  selector: 'copyDone',
  templateUrl: 'copyDone.html',
})
export class CopyDone {}




interface photosInterface {
  index:number;
  upload_time:string;
  iso:number;
  shutter_speed:string;
  aperture:string;
  content:string;
  photo_thumbnail:string;
  photo_show:string;
  user:user;
}
interface user {
  nickname:string;
  image_thumbnail:string;
}